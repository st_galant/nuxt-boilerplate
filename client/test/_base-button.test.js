import { render } from '@testing-library/vue'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'

import Button from '@/components/_base-button'

test('renders button with slot', () => {
  const text = 'Click me!'

  const { getByRole } = render(Button, {
    slots: {
      default: text,
    },
  })

  const button = getByRole('button')

  expect(button).toHaveTextContent(text)
})

test('emits click event when button is clicked', () => {
  const text = 'Click me again!'

  const { getByRole, emitted } = render(Button, {
    slots: {
      default: text,
    },
  })

  userEvent.click(getByRole('button'))
  expect(emitted()).toHaveProperty('click')
})
