import { render, fireEvent } from '@testing-library/vue'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'

import InputRange from '~/components/_base-input-range'

beforeAll((done) => {
  // mock getBoundingClientRect for jsdom
  window.HTMLElement.prototype.getBoundingClientRect = jest.fn(() => ({
    left: 0,
    right: 200,
    top: 0,
    bottom: 10,
  }))
  done()
})

describe('Input Range', () => {
  const scopedSlots = {
    track: `<div
    style="width: 200px; height: 10px;"
    data-testid="track-slot-used"
    >{{props.drag}}</div>`,
    thumb: `<div
    data-testid="thumb-slot-used"
    :data-drag="props.drag"
    style="width: 20px; height: 20px;"
    ></div>`,
  }

  const props = {
    min: 0,
    max: 100,
    value: 33,
  }

  test('renders range with two slots: track and thumb', () => {
    const { getByRole, getByTestId } = render(InputRange, {
      scopedSlots,
    })
    getByRole('range')
    getByTestId('track-slot-used')
    getByTestId('thumb-slot-used')
  })

  test('should render with value prop', () => {
    const { container } = render(InputRange, {
      scopedSlots,
      props,
    })

    const thumb = container.querySelector('.thumb')
    expect(thumb.style.left).toEqual('33%')
  })

  test('should render with default value: 0', () => {
    const { container } = render(InputRange, {
      scopedSlots,
      props: {
        min: -100,
        max: 100,
      },
    })

    const thumb = container.querySelector('.thumb')
    expect(thumb.style.left).toEqual('50%')
  })

  test('should emit input event with value on click', () => {
    const { getByTestId, emitted } = render(InputRange, {
      scopedSlots,
      props,
    })

    const track = getByTestId('track-slot-used')
    const rect = track.getBoundingClientRect()
    userEvent.click(track, {
      clientX: rect.left + 0.75 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    expect(emitted().input).toEqual([[75]])
  })

  test('should clamp value to minimum', () => {
    const { emitted, container } = render(InputRange, {
      scopedSlots,
      props: {
        value: -1,
      },
    })
    const thumb = container.querySelector('.thumb')
    expect(thumb.style.left).toEqual('0%')
    expect(emitted().input).toEqual([[0]])
  })

  test('should clamp value to maximum', () => {
    const { emitted, container } = render(InputRange, {
      scopedSlots,
      props: {
        value: 1000,
      },
    })
    const thumb = container.querySelector('.thumb')
    expect(thumb.style.left).toEqual('100%')
    expect(emitted().input).toEqual([[100]])
  })

  test('tabindex focus and change value by keyboard', async () => {
    const { emitted, container, updateProps } = render(InputRange, {
      scopedSlots,
      props: {
        tabindex: 1,
        min: 0,
        max: 2,
        value: 0,
        step: 1,
      },
    })

    const track = container.querySelector('.track')
    userEvent.tab()
    expect(track).toHaveFocus()

    await fireEvent.keyDown(document.activeElement, {
      key: 'ArrowRight',
      code: 'ArrowRight',
      keyCode: 39,
    })

    expect(emitted().input[0]).toEqual([1])
    await updateProps({ value: 1 })

    await fireEvent.keyDown(document.activeElement, {
      key: 'ArrowRight',
      code: 'ArrowRight',
      keyCode: 39,
    })

    expect(emitted().input[1]).toEqual([2])
    await updateProps({ value: 2 })

    await fireEvent.keyDown(document.activeElement, {
      key: 'ArrowRight',
      code: 'ArrowRight',
      keyCode: 39,
    })
    expect(emitted().input[2]).toBeUndefined()
  })

  test('should emit input event with value on drag and toggle drap prop', () => {
    const { getByTestId, emitted } = render(InputRange, {
      scopedSlots,
      props,
    })

    const track = getByTestId('track-slot-used')
    const rect = track.getBoundingClientRect()
    fireEvent.mouseDown(track, {
      clientX: rect.left + 0.25 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.75 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseMove(track, {
      clientX: rect.left + 1.25 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseMove(track, {
      clientX: rect.left - 0.25 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.25 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseUp(track, {
      clientX: rect.left + 0.5 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.75 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    expect(emitted().input).toEqual([[25], [75], [100], [0], [25]])
  })
  test('should emit integer values with step 2 and sticky threshold factor of 0.33', () => {
    const { getByTestId, emitted } = render(InputRange, {
      scopedSlots,
      props: {
        min: -4,
        max: 4,
        value: 0,
        step: 2,
        threshold: 0.33,
      },
    })
    //  (-4)---(-2)---(0)---(2)---(4)
    //  --------------^-------------
    const track = getByTestId('track-slot-used')
    const rect = track.getBoundingClientRect()
    fireEvent.mouseDown(track, {
      clientX: rect.left + (0.5 - 0.125) * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseDown(track, {
      clientX: rect.left + (0.5 - 0.25 * 0.67) * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseDown(track, {
      clientX: rect.left + (1 - 0.124) * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseUp(track, {
      clientX: rect.left + 0.5 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    expect(emitted().input).toEqual([[-2], [4]])
  })
  test('should emit float values with step 1/3 and sticky threshold factor of 0.33', () => {
    const { getByTestId, emitted } = render(InputRange, {
      scopedSlots,
      props: {
        min: 0,
        max: 1,
        value: 0,
        step: 1 / 3,
        threshold: 0.33,
      },
    })
    //  (0)---(0.33)---(0.66)---(1)
    //  -^-------------
    const track = getByTestId('track-slot-used')
    const rect = track.getBoundingClientRect()
    fireEvent.mouseDown(track, {
      clientX: rect.left + 0.125 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseDown(track, {
      clientX: rect.left + 0.333 * 0.68 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseDown(track, {
      clientX: rect.left + 0.85 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseUp(track, {
      clientX: rect.left + 0.5 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    expect(emitted().input).toEqual([[1 / 3], [1]])
  })

  test('present in form', () => {
    const Form = {
      template: `
      <form role='form'>
        <input-range name="range" :value="55" :max="100" />
      </form>
    `,
      components: { InputRange },
    }
    const { getByRole } = render(Form)

    const form = getByRole('form')
    expect(form.elements.range.value).toEqual('55')
  })
})
