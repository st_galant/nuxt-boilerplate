import { render } from '@testing-library/vue'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'

import Checkbox from '~/components/_base-checkbox'

test('renders checkbox with scoped slot', () => {
  const { getByText } = render(Checkbox, {
    props: {
      checked: true,
    },
    scopedSlots: {
      default: '<span v-if="props.checked">Scoped Label</span>',
    },
  })

  getByText('Scoped Label')
})

test('checkbox default is off', () => {
  const text = 'Click me!'

  const { getByLabelText } = render(Checkbox, {
    slots: {
      default: text,
    },
  })

  const c = getByLabelText(text)

  expect(c).not.toBeChecked()
})

test('checkbox is checked', () => {
  const text = 'Click me!'

  const { getByRole } = render(Checkbox, {
    props: {
      checked: true,
    },
    slots: {
      default: text,
    },
  })

  const c = getByRole('checkbox')

  expect(c).toBeChecked()
})

test('emits toggle event when label is clicked', () => {
  const text = 'Click me again!'

  const { getByRole, emitted } = render(Checkbox, {
    slots: {
      default: text,
    },
  })

  userEvent.click(getByRole('checkbox'))
  expect(emitted().toggle[0]).toEqual([true])
})

test('emits toggle event with inverted status', () => {
  const text = 'Click me again!'

  const { getByRole, emitted } = render(Checkbox, {
    props: {
      checked: true,
    },
    slots: {
      default: text,
    },
  })

  userEvent.click(getByRole('checkbox'))
  expect(emitted().toggle[0]).toEqual([false])
})

test('emits toggle event when click label', () => {
  const text = 'Click me again!'

  const { getByText, emitted } = render(Checkbox, {
    props: {
      checked: true,
    },
    slots: {
      default: text,
    },
  })

  userEvent.click(getByText(text))
  expect(emitted().toggle[0]).toEqual([false])
})
