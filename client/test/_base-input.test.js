import { render } from '@testing-library/vue'
import '@testing-library/jest-dom'
import userEvent from '@testing-library/user-event'

import Input from '~/components/_base-input'

test('renders input with placeholder', () => {
  const placeholder = 'nice placeholder text'
  const { getByPlaceholderText } = render(Input, {
    props: {
      placeholder,
    },
  })

  getByPlaceholderText(placeholder)
})

test('renders input with text', () => {
  const text = 'Predefined text'

  const { getByDisplayValue } = render(Input, {
    props: {
      text,
    },
  })

  getByDisplayValue(text)
})

test('emits input event on typing', () => {
  const text = 'Robot text'
  const printedText = []

  for (let i = 0; i < text.length; i++) {
    printedText.push([text.substring(0, i + 1)])
  }

  const { getByRole, emitted } = render(Input)

  userEvent.type(getByRole('textbox'), text)
  expect(emitted().input).toEqual(printedText)
  expect(getByRole('textbox')).toHaveValue(text)
})
