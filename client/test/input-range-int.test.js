import { render, fireEvent } from '@testing-library/vue'
import '@testing-library/jest-dom'

import InputRange from '~/components/input-range-int'
import BaseInputRange from '~/components/_base-input-range'

beforeAll((done) => {
  // mock getBoundingClientRect for jsdom
  window.HTMLElement.prototype.getBoundingClientRect = jest.fn(() => ({
    left: 0,
    right: 200,
    top: 0,
    bottom: 10,
  }))
  done()
})

describe('Input Range Integer', () => {
  const props = {
    min: 0,
    max: 10,
    value: 0,
    threshold: 0.33,
  }

  // const BaseInputRange = (...v) => ({
  //   template: '',
  //   render(h) {},
  //   mounted() {
  //     v.forEach((v) => {
  //       this.$emit('input', v)
  //     })
  //   },
  // })
  test('should emit input event with rounded value from base-input-range component', () => {
    const { emitted, getByTestId } = render(InputRange, {
      props,
      components: { BaseInputRange },
      // stubs: {
      //   'base-input-range': BaseInputRange(0.1, 3.3, 4.6, 9.1),
      // },
    })

    const track = getByTestId('track')
    const rect = track.getBoundingClientRect()
    fireEvent.mouseDown(track, {
      clientX: rect.left + 0 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.01 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.33 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.46 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })
    fireEvent.mouseMove(track, {
      clientX: rect.left + 0.91 * rect.right,
      clientY: rect.top + 0.5 * rect.bottom,
    })

    expect(emitted().input).toEqual([[3], [5], [9]])
  })
})
