/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    extend: {
      backgroundColor: {
        br: 'var(--brand-color, #ffffff)',
        'br-l': 'var(--brand-color-light, #ffffff)',
        'br-d': 'var(--brand-color-dark, #ffffff)',
        p: 'var(--background-color, #ffffff)',
        s: 'var(--background-color-secondary, #ffffff)',
        t: 'var(--text-color, #ffffff)',
        't-s': 'var(--text-color-secondary, #ffffff)',
        scs: 'var(--success-color)',
        'scs-c': 'var(--success-contrast-color)',
        wrn: 'var(--warning-color)',
        'wrn-c': 'var(--warning-contrast-color)',
        dan: 'var(--danger-color)',
        'dan-c': 'var(--danger-contrast-color)',
        act: 'var(--action-bg-color)',
        'act-s': 'var(--action-bg-color-secondary)',
      },
      textColor: {
        br: 'var(--brand-color, #ffffff)',
        'br-l': 'var(--brand-color-light, #ffffff)',
        'br-d': 'var(--brand-color-dark, #ffffff)',
        p: 'var(--text-color, #ffffff)',
        s: 'var(--text-color-secondary, #ffffff)',
        bg: 'var(--background-color, #ffffff)',
        'bg-s': 'var(--background-color-secondary, #ffffff)',
        scs: 'var(--success-contrast-color)',
        'scs-c': 'var(--success-color)',
        wrn: 'var(--warning-contrast-color)',
        'wrn-c': 'var(--warning-color)',
        dan: 'var(--danger-contrast-color)',
        'dan-c': 'var(--danger-color)',
        act: 'var(--action-text-color)',
        'act-s': 'var(--action-text-color-secondary)',
      },
      borderColor: {
        br: 'var(--brand-color, #ffffff)',
        'br-l': 'var(--brand-color-light, #ffffff)',
        'br-d': 'var(--brand-color-dark, #ffffff)',
        bg: 'var(--background-color, #ffffff)',
        'bg-s': 'var(--background-color-secondary, #ffffff)',
        t: 'var(--text-color, #ffffff)',
        't-s': 'var(--text-color-secondary, #ffffff)',
        scs: 'var(--success-color)',
        'scs-c': 'var(--success-contrast-color)',
        wrn: 'var(--warning-color)',
        'wrn-c': 'var(--warning-contrast-color)',
        dan: 'var(--danger-color)',
        'dan-c': 'var(--danger-contrast-color)',
        act: 'var(--action-text-color)',
        'act-s': 'var(--action-text-color-secondary)',
      },
    },
    variants: {},
    plugins: [],
  },
  purge: ['../**/*.html', '../**/*.vue'],
}
