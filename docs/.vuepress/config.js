module.exports = {
  title: 'Title',
  description: 'Description',
  host: 'localhost',
  themeConfig: {
    sidebar: [['/', 'Introduction'], ['Colors']],
  },
  locales: {
    '/': {
      lang: 'en-US',
      title: 'Nuxt boilerplate',
      description: 'Boilerplate project for NuxtJS fast start',
    },
    '/ru/': {
      lang: 'ru-RU',
      title: 'Шаблон Nuxt',
      description: 'Шаблонный проект NuxtJS для бысрого старта',
    },
  },
}
