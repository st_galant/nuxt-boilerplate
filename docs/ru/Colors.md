# Цветовая тема

[[toc]]

## Темизация и система цветов

Темизация цветов расширяет систему классов TailwindCSS для возможности использования переменных CSS. Например:

```css
.bg-p {
  background-color: var(--bg-primary, #ffffff);
}
```

## Реализованные классы и переменные

| Название                     | Класс CSS  | Переменная CSS                |
| ---------------------------- | ---------- | ----------------------------- |
| Background Brand color       | bg-br      | --brand-color                 |
| Background Brand light color | bg-br-d    | --brand-color-light           |
| Background Brand dark color  | bg-br-l    | --brand-color-dark            |
| Text Brand color             | text-br    | --brand-color                 |
| Text Brand light color       | text-br-d  | --brand-color-light           |
| Text Brand dark color        | text-br-l  | --brand-color-dark            |
| Primary background color     | bg-p       | --bg-primary                  |
| Primary text color           | text-p     | --text-primary                |
| Primary border color         | border-p   | --border-primary              |
| Secondary background color   | bg-s       | --bg-secondary                |
| Secondary text color         | text-s     | --text-secondary              |
| Secondary border color       | border-s   | --border-secondary            |
| Success background color     | bg-scs     | --bg-success                  |
| Success text color           | text-scs   | --text-success                |
| Success border color         | border-scs | --border-success              |
| Warning background color     | bg-wrn     | --bg-warn                     |
| Warning text color           | text-wrn   | --text-warn                   |
| Warning border color         | border-wrn | --border-warn                 |
| Danger background color      | bg-dan     | --bg-danger                   |
| Danger text color            | text-dan   | --text-danger                 |
| Danger border color          | border-dan | --border-danger               |
| Action background color      | bg-act     | --action-bg-color             |
| Action secondary color       | bg-act-s   | --action-bg-color-secondary   |
| Action text color            | text-act   | --action-text-color           |
| Action text secondary color  | text-act-s | --action-text-color-secondary |
