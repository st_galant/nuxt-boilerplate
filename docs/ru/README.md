# Шаблонный прект NuxtJS

Для полной документации по nuxt см. <https://nuxtjs.org/>

Добавлено:

- Раздельные папки для клиента и сервера.
- Настройки для VSCode.
- Немного изменений в конфигурациях для eslint, prettier, stylelint.
- Регистрация базовых компонентов.
- Интеграция с TailwindCSS.
- [Цветовая схема](Colors.md)

Убрано все лишнее.
